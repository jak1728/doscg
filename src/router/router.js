import React from 'react';

import Layout from '../template/App';
import HomePage from '../pages/home';
import AboutPage from '../pages/about';
import ResumePage from '../pages/resume';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

const RouterList = () => (
  <Router>
    <Switch>
      <Route exact path="/DOSCG" render={() => <Layout><HomePage /></Layout>} />
      <Route exact path="/DOSCG/about" render={() => <Layout><AboutPage /></Layout>} />
      <Route exact path="/DOSCG/resume" render={() => <Layout><ResumePage /></Layout>} />
      <Route component={NotFound} />
    </Switch>
  </Router>  
)

const Status = ({ code, children }) => (
  <Route render={({ staticContext }) => {
    if (staticContext)
      staticContext.status = code
    return children
  }}/>
)

const NotFound = () => (
  <Status code={404}>
    <div className="theme1">
      <h1>Sorry, can't find that.</h1>
    </div>
  </Status>
)

export default RouterList;