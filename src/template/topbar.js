import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import './topbar.css';

class Topbar extends Component {
    render(){
        return (
            <>
            <ul className="topbar">
                <li><NavLink to="/DOSCG">home</NavLink></li>
                <li><NavLink to="/DOSCG/resume">resume</NavLink></li>
                <li><NavLink to="/DOSCG/about">about</NavLink></li>
            </ul>
            </>
            );
    }
}

export default Topbar;
