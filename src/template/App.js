import React, { Component } from 'react';
import './App.css';
import Topbar from './topbar'; 
import Footer from './footer';

class App extends Component {
  render(){
    return (
    <div className="App">
      <Topbar />
      <div>{this.props.children}</div>
      <Footer />
    </div>
    )
  }
}

export default App;
