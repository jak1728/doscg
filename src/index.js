import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RouterList from './router/router';
import {
    BrowserRouter
} from 'react-router-dom';

ReactDOM.render(
  <BrowserRouter>
      <RouterList />
  </BrowserRouter>,
  document.getElementById('root')
);