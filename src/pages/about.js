import React, { Component } from 'react';
import axios from 'axios';

class About extends Component {

    constructor(props){
        super(props);

        this.state = {
            addr : null
        }
    }

    componentDidMount(){

        var $this = this;

        axios.get('../data/about.json')
          .then(function (response) {
            $this.setState({
                addr : response.data.addr
            })
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    render(){
        return (
            <>
                <h1>about page</h1>
                <div className="block">{this.state.addr}</div>
            </>
        );
    }
}

export default About;
