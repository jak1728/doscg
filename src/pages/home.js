import React, { Component } from 'react';
import axios from 'axios';
import './home.css';

class Home extends Component {

    constructor(props){
        super(props);

        this.state = {
            intro : null
        }
    }

    componentDidMount(){

        var $this = this;

        axios.get('../data/home.json')
          .then(function (response) {
            $this.setState({
                intro : response.data.desc
            })
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    render(){
        return (
        <>
            <h1>Home</h1>
            <div className="block">{this.state.intro}</div>
        </>
        );
    }
}

export default Home;
