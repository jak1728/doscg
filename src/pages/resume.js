import React, { Component } from 'react';
import axios from 'axios';

class Resume extends Component {

    constructor(props){
        super(props);

        this.state = {
            exp : null
        }
    }

    componentDidMount(){

        var $this = this;

        axios.get('../data/resume.json')
          .then(function (response) {
            $this.setState({
                exp : response.data
            })
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    render(){

        var rows = this.state.exp;

        return (
            <>
                <h1>Resume page</h1>
                {rows != null && rows.map((list,index) => (
                    <div key={index} className="block">
                        <div>Name : {list.name}</div>
                        <div>Job : {list.job}</div>
                        <div>Period : {list.period}</div>
                        <div>Jobdesc : {list.jobdesc.map((listsub,indexsub) => (
                            <div key={indexsub}>{listsub}</div>
                        ))}</div>
                        <div>------------------------------------------------------------</div>
                    </div>
                ))}
            </>
            );
    }
}

export default Resume;
